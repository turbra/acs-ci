# acs-ci
## Using GitLab-Runners with Red Hat Advanced Cluster Security for Kubernetes 

## Getting started
### [roxctl command reference](https://docs.openshift.com/acs/4.5/cli/command-reference/roxctl-image.html)
| Command | Description |
| ------ | ------ |
|    check    |Check images for build time policy violations, and report them.        |
|    scan    |Scan the specified image, and return the scan results.

        


#### Shell Executor - refer to `shell-scan` branch
- ACS image check is performed via roxctl cli fetched from central
```yaml
🔐Scan the image:
  stage: scan
  script:
    - 'curl -k -H "Authorization: Bearer $ROX_API_TOKEN" "$STACKROX_CENTRAL_HOST":443/api/cli/download/roxctl-linux -o roxctl && chmod +x ./roxctl'
    - ./roxctl --insecure-skip-tls-verify -e $STACKROX_CENTRAL_HOST:443 image check --image=repo.example.com/namespace/escape:1.0
  tags:
    - runner-shell
  only:
    - shell-scan
  allow_failure: true
```

#### Docker Executor - refer to `container-scan` branch
- ACS image check is performed via `rhacs-roxctl` container image
```yaml
🔐Scan the image:
  image:
    name: registry.redhat.io/advanced-cluster-security/rhacs-roxctl-rhel8
  stage: scan
  script:
    - ROX_API_TOKEN=$ROX_API_TOKEN
    - STACKROX_CENTRAL_HOST=$STACKROX_CENTRAL_HOST
    - roxctl --insecure-skip-tls-verify image check -e $STACKROX_CENTRAL_HOST:443 --image=repo.example.com/namespace/escape:1.0
  tags:
    - docker-runner
  only:
    - container-scan
  allow_failure: true
```

## Variables required:
- ROX_API_TOKEN
- STACKROX_CENTRAL_HOST
- DOCKER_REPO (private or public)
  - Replace `repo.example.com/namespace`
